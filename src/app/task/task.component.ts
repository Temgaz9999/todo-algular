import { Component, OnInit } from '@angular/core';
import { TasksService } from 'src/shared/tasks.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  isLoading = true;

  constructor(public tasksService: TasksService) { }

  ngOnInit(): void {
    this.tasksService.fetchTasks()
      .subscribe(() => {
        this.isLoading = false;
      });
  }

  trackByIdx(index: number, obj: any): any {
    return index;
  }

  removeTask(id: number): void {
    this.tasksService.removeTask(id).subscribe(
      (response) => {
        if (response.status === 200) {
          this.tasksService.tasks = this.tasksService.tasks.filter(task => task.id !== id);
          this.tasksService.filteredTasks = this.tasksService.tasks;
        }
      }
    );
  }

  completeTask(id: number): void {
    const index = this.tasksService.tasks.findIndex(taskA => taskA.id === id);
    const task = this.tasksService.tasks[index];
    task.isActive = false;
    task.isDone = true;
    this.tasksService.updateTask(id, task).subscribe();
    this.tasksService.filteredTasks = this.tasksService.tasks;
  }

  cancelTask(id: number): void {
    const index = this.tasksService.tasks.findIndex(taskA => taskA.id === id);
    const task = this.tasksService.tasks[index];
    task.isActive = false;
    task.isCancelled = true;
    this.tasksService.updateTask(id, task).subscribe();
    this.tasksService.filteredTasks = this.tasksService.tasks;
  }

  updateText(id: number): void {
    const index = this.tasksService.tasks.findIndex(taskA => taskA.id === id);
    const task = this.tasksService.tasks[index];
    task.text = this.tasksService.filteredTasks[index].text;
    this.tasksService.updateTask(id, task).subscribe();
    this.tasksService.filteredTasks = this.tasksService.tasks;
  }
}
