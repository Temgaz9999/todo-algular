import { Component, OnInit } from '@angular/core';
import { Task, TasksService } from 'src/shared/tasks.service';

@Component({
  selector: 'app-task-filter',
  templateUrl: './task-filter.component.html',
  styleUrls: ['./task-filter.component.css']
})
export class TaskFilterComponent implements OnInit {

  constructor(public tasksService: TasksService) { }
  isTasksSortedUp = true;
  completeCheck = false;
  highCheck = false;
  mediumCheck = false;
  lowCheck = false;

  initialTasks = this.tasksService.tasks;

  ngOnInit(): void {
  }

  getWeightForNullDate: any = (dateA: Date, dateB: Date) => {
    if (dateA === null && dateB === null) {
      return 0;
    }

    if (dateA === null) {
      return 1;
    }

    if (dateB === null) {
      return -1;
    }

    return null;
  }

  sortTaskUp: any = (taskA: Task, taskB: Task) => {
    const weight = this.getWeightForNullDate(taskA.creationDate, taskB.creationDate);

    if (weight !== null) {
      return weight;
    }
    const taskBTime: Date = new Date(taskB.creationDate);
    const taskATime: Date = new Date(taskA.creationDate);
    return taskATime.getTime() - taskBTime.getTime();
}

 sortTaskDown: any = (taskA: Task, taskB: Task) => {
    const weight = this.getWeightForNullDate(taskA.creationDate, taskB.creationDate);

    if (weight !== null) {
      return weight;
    }
    const taskBTime: Date = new Date(taskB.creationDate);
    const taskATime: Date = new Date(taskA.creationDate);
    return taskBTime.getTime() - taskATime.getTime();
}

filterButtonsDisabler(): void {
  if (this.highCheck) {
    this.highCheck = false;
  }
  if (this.mediumCheck) {
    this.mediumCheck = false;
  }
  if (this.lowCheck) {
    this.lowCheck = false;
  }
  if (this.completeCheck) {
    this.completeCheck = false;
  }
}

sort(): void {
  if (this.isTasksSortedUp) {
    this.tasksService.filteredTasks.sort(this.sortTaskDown);
    this.tasksService.tasks.sort(this.sortTaskDown);
  } else {
    this.tasksService.filteredTasks.sort(this.sortTaskUp);
    this.tasksService.tasks.sort(this.sortTaskUp);
  }
  this.isTasksSortedUp = !this.isTasksSortedUp;
  this.filterButtonsDisabler();
}



filter(): void {
  let total: Task[] = [];
  const filteringTasks = this.tasksService.tasks.slice();

  function unique(arr: Task[]): Task[] {
    const result: Task[] = [];

    for (const str of arr) {
      if (!result.includes(str)) {
        result.push(str);
      }
    }

    return result;
  }

  if (this.completeCheck) {
    if (this.highCheck) {
        filteringTasks.forEach(item => {
            if (!item.isActive && item.priority === 'High') {total.push(item); }
        });
    } else if (this.mediumCheck) {
        filteringTasks.forEach(item => {
            if (!item.isActive && item.priority === 'Medium') {total.push(item); }
        });
    } else if (this.lowCheck) {
        filteringTasks.forEach(item => {
            if (!item.isActive && item.priority === 'Low') {total.push(item); }
        });
    } else {
        filteringTasks.forEach(item => {
            if (!item.isActive) {total.push(item); }
        });
    }
  }

  if (this.highCheck) {
    if (this.completeCheck) {
        filteringTasks.forEach(item => {
            if (!item.isActive && item.priority === 'High') {total.push(item); }
        });
    } else {
        filteringTasks.forEach(item => {
            if (item.priority === 'High') {total.push(item); }
        });
    }
  }

  if (this.mediumCheck) {
    if (this.completeCheck) {
        filteringTasks.forEach(item => {
            if (!item.isActive && item.priority === 'Medium') {total.push(item); }
        });
    } else {
        filteringTasks.forEach(item => {
            if (item.priority === 'Medium') {total.push(item); }
        });
    }
  }

  if (this.lowCheck) {
    if (this.completeCheck) {
        filteringTasks.forEach(item => {
            if (!item.isActive && item.priority === 'Low') {total.push(item); }
        });
    } else {
        filteringTasks.forEach(item => {
            if (item.priority === 'Low') {total.push(item); }
        });
    }
  }

  const isChecked = this.completeCheck || this.highCheck || this.mediumCheck || this.lowCheck;


  if (total.length === 0) {
    if (!isChecked) {
      total = this.tasksService.tasks.slice();
      this.tasksService.filterTasks(total);
    } else {this.tasksService.filterTasks(total); }
  } else {
    total = unique(total);
    this.tasksService.filterTasks(total);
  }


}

clickHandler(e: any): void {
  if (e.target.checked === true) {
    switch (e.target.id) {
      case 'checkbox__high':  this.highCheck = true; break;
      case 'checkbox__medium':  this.mediumCheck = true; break;
      case 'checkbox__low':  this.lowCheck = true; break;
      case 'checkbox__completed':  this.completeCheck = true; break;
    }
  }
  else {
    switch (e.target.id) {
      case 'checkbox__high':  this.highCheck = false; break;
      case 'checkbox__medium':  this.mediumCheck = false; break;
      case 'checkbox__low':  this.lowCheck = false; break;
      case 'checkbox__completed':  this.completeCheck = false; break;
    }
  }
}

}
