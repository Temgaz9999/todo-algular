import { Component, OnInit } from '@angular/core';
import { Task, TasksService } from 'src/shared/tasks.service';

@Component({
  selector: 'app-task-creator',
  templateUrl: './task-creator.component.html',
  styleUrls: ['./task-creator.component.css']
})
export class TaskCreatorComponent implements OnInit {
  text = '';
  priority = 'High';

  constructor(private tasksService: TasksService) { }

  ngOnInit(): void {
  }

  addTask(): void {
    if (this.text) {
      const task: Task = {
        text: this.text,
        id: Date.now(),
        isActive: true,
        isCancelled: false,
        isDone: false,
        priority: this.priority,
        creationDate: new Date(),
        finishDate: new Date()
      };
      this.tasksService.addTask(task).subscribe(

        (response) => {
          if (response.status === 200) {
            const responseBody: any | null = response.body;
            const responseId = responseBody.id;
            task.id = responseId;
            this.tasksService.tasks.push(task);
            this.tasksService.filteredTasks = this.tasksService.tasks;
            this.text = '';
          }
        }
      );
    }
    this.tasksService.fetchTasks().subscribe();
  }

}
