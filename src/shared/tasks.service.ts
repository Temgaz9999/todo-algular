import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import {tap} from 'rxjs/operators';

export interface Task {
    id?: any;
    text: string;
    creationDate: Date;
    finishDate: Date;
    isActive: boolean;
    isDone: boolean;
    isCancelled: boolean;
    priority: string;
  }

@Injectable({providedIn: 'root'})
export class TasksService {
  public tasks: Task[] = [];
  public filteredTasks: Task[] = [];

  constructor(private http: HttpClient) {}

  fetchTasks(): Observable<Task[]> {
    return this.http.get<Task[]>('http://127.0.0.1:3000/items')
      .pipe(tap(tasks => {this.tasks = tasks; this.filteredTasks = this.tasks; }));
  }

  removeTask(id: number): Observable<HttpResponse<object>> {
    return this.http.delete(`http://127.0.0.1:3000/items/${id}`, {observe: 'response'});
  }

  updateTask(id: number, task: Task): Observable<HttpResponse<object>> {
    return this.http.put(`http://127.0.0.1:3000/items/${id}`, task, {observe: 'response'});
  }

  addTask(task: Task): Observable<HttpResponse<object>> {
    return this.http.post('http://127.0.0.1:3000/items', task, {observe: 'response'});
  }

  filterTasks(tasks: Task[]): void {
    this.filteredTasks = tasks;
  }
}
